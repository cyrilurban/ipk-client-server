/**
 * @file	ftrestd.h
 * @author	CYRIL URBAN
 * @date:	2017-03-14
 * @brief	The client/server application
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <ctype.h>
#include <stdbool.h>
#include <time.h>
#include <sys/stat.h>

// size of message buffer
#define BUFF_SIZE 2000

// struct with argumetns
typedef struct {
	char command[4];
	char remotePath[100];
	char localPath[100];
	char host[100];
	int port;
} arguments;

arguments parameterProcessing(int argc , char *argv[]);
void put(int sock, arguments a);
void lst(int sock, arguments a);
void mkd(int sock, arguments a);
void rmd(int sock, arguments a);
void del(int sock, arguments a);
void get(int sock, arguments a);