/**
 * @file	ftrest.c
 * @author	CYRIL URBAN
 * @date:	2017-03-14
 * @brief	The client/server application
 */

#include "ftrest.h"

/**
 * @brief      The main function of client
 *
 * @param[in]  argc  The argc
 * @param      argv  The argv
 *
 * @return     0 or 1
 */
int main(int argc , char *argv[])
{
	// set command, remote and local path from arguments, call function parameterProcessing
	arguments a;
	a = parameterProcessing(argc , argv);

	int sock;
	struct sockaddr_in server;
	char message[BUFF_SIZE];

	// Create socket
	sock = socket(AF_INET , SOCK_STREAM , 0);
	if (sock == -1) {
		fprintf(stderr, "%s", "Unknown error.\n");
		exit(1);
	}

	// Prepare the sockaddr_in structure
	server.sin_addr.s_addr = inet_addr(a.host);
	server.sin_family = AF_INET;
	server.sin_port = htons(a.port);

	// Connect to remote server
	if (connect(sock , (struct sockaddr *)&server , sizeof(server)) < 0) {
		fprintf(stderr, "%s", "Unknown error.\n");
		exit(1);
	}

	// COMAND
	// put
	if (strcmp(a.command, "put") == 0) {
		put(sock, a);
	}
	// lst
	if (strcmp(a.command, "lst") == 0) {
		lst(sock, a);
	}
	// mkd
	if (strcmp(a.command, "mkd") == 0) {
		mkd(sock, a);
	}
	// rmd
	if (strcmp(a.command, "rmd") == 0) {
		rmd(sock, a);
	}
	// del
	if (strcmp(a.command, "del") == 0) {
		del(sock, a);
	}
	// get
	if (strcmp(a.command, "get") == 0) {
		get(sock, a);
	}

	// Close sock and successfully exit
	close(sock);
	return 0;
}

/**
 * @brief      The mkd function makes directory on server
 *
 * @param[in]  sock  The socket
 * @param[in]  a     The argument
 */
void mkd(int sock, arguments a) {

	// HTTP: Date
	char date[1000];
	time_t now = time(0);
	struct tm tm = *gmtime(&now);
	strftime(date, sizeof date, "%a, %d %b %Y %H:%M:%S %Z", &tm);

	// prepare HTTP request
	char httpHeader[BUFF_SIZE];
	sprintf(httpHeader,
	        "PUT %s?type=folder HTTP/1.1\r\nDate: %s\r\nAccept: text/plain\r\nAccept-Encoding: identity\r\n\r\n",
	        a.remotePath, date);

	// send HTTP request to server
	if ( send(sock , httpHeader , strlen(httpHeader) , 0) < 0) {
		fprintf(stderr, "%s", "Unknown error.\n");
		exit(1);
	}

	// Receive a message with response from server
	char message[BUFF_SIZE];
	recv(sock , message , BUFF_SIZE , 0);

	// read code from HTTP response, if not OK - exit with certain error
	char code[12];
	strncpy(code, message, 12);
	code[12] = '\0';

	if (strcmp(code, "HTTP/1.1 400") == 0) {
		fprintf(stderr, "%s", "Already exists.\n");
		exit(1);
	}
	if (strcmp(code, "HTTP/1.1 401") == 0) {
		fprintf(stderr, "%s", "User Account Not Found.\n");
		exit(1);
	}
}

/**
 * @brief      The rmd function deletess directory on server
 *
 * @param[in]  sock  The socket
 * @param[in]  a     The arguments
 */
void rmd(int sock, arguments a) {

	// HTTP: Date
	char date[1000];
	time_t now = time(0);
	struct tm tm = *gmtime(&now);
	strftime(date, sizeof date, "%a, %d %b %Y %H:%M:%S %Z", &tm);

	// prepare HTTP request
	char httpHeader[BUFF_SIZE];
	sprintf(httpHeader,
	        "DELETE %s?type=folder HTTP/1.1\r\nDate: %s\r\nAccept: text/plain\r\nAccept-Encoding: identity\r\n\r\n",
	        a.remotePath, date);

	// send HTTP request to server
	if ( send(sock , httpHeader , strlen(httpHeader) , 0) < 0) {
		fprintf(stderr, "%s", "Unknown error.\n");
		exit(1);
	}

	// Receive a message with response from server
	char message[BUFF_SIZE];
	recv(sock , message , BUFF_SIZE , 0);

	// read code from HTTP response, if not OK - exit with certain error
	char code[12];
	strncpy(code, message, 12);
	code[12] = '\0';

	if (strcmp(code, "HTTP/1.1 400") == 0) {
		fprintf(stderr, "%s", "Not a directory.\n");
		exit(1);
	}
	if (strcmp(code, "HTTP/1.1 404") == 0) {
		fprintf(stderr, "%s", "Directory not found.\n");
		exit(1);
	}
	if (strcmp(code, "HTTP/1.1 401") == 0) {
		fprintf(stderr, "%s", "User Account Not Found.\n");
		exit(1);
	}
	if (strcmp(code, "HTTP/1.1 409") == 0) {
		fprintf(stderr, "%s", "Directory not empty.\n");
		exit(1);
	}
	if (strcmp(code, "HTTP/1.1 422") == 0) {
		fprintf(stderr, "%s", "The user cannot be deleted.\n");
		exit(1);
	}
}

/**
 * @brief      The del function deletes file on server
 *
 * @param[in]  sock  The socket
 * @param[in]  a     The arguments
 */
void del(int sock, arguments a) {

	// HTTP: Date
	char date[1000];
	time_t now = time(0);
	struct tm tm = *gmtime(&now);
	strftime(date, sizeof date, "%a, %d %b %Y %H:%M:%S %Z", &tm);

	// prepare HTTP request
	char httpHeader[BUFF_SIZE];
	sprintf(httpHeader,
	        "DELETE %s?type=file HTTP/1.1\r\nDate: %s\r\nAccept: text/plain\r\nAccept-Encoding: identity\r\n\r\n",
	        a.remotePath, date);

	// send HTTP request to server
	if ( send(sock , httpHeader , strlen(httpHeader) , 0) < 0) {
		fprintf(stderr, "%s", "Unknown error.\n");
		exit(1);
	}

	// Receive a message with response from server
	char message[BUFF_SIZE];
	recv(sock , message , BUFF_SIZE , 0);

	// read code from HTTP response, if not OK - exit with certain error
	char code[12];
	strncpy(code, message, 12);
	code[12] = '\0';

	if (strcmp(code, "HTTP/1.1 404") == 0) {
		fprintf(stderr, "%s", "File not found.\n");
		exit(1);
	}
	if (strcmp(code, "HTTP/1.1 400") == 0) {
		fprintf(stderr, "%s", "Not a file.\n");
		exit(1);
	}
	if (strcmp(code, "HTTP/1.1 401") == 0) {
		fprintf(stderr, "%s", "User Account Not Found.\n");
		exit(1);
	}
}

/**
 * @brief      The function finds out list segments (ls) and result is written
 *             to the stdout.
 *
 * @param[in]  sock  The socket
 * @param[in]  a     The arguments
 */
void lst(int sock, arguments a) {

	// HTTP: Date
	char date[1000];
	time_t now = time(0);
	struct tm tm = *gmtime(&now);
	strftime(date, sizeof date, "%a, %d %b %Y %H:%M:%S %Z", &tm);

	// prepare HTTP request
	char httpHeader[BUFF_SIZE];
	sprintf(httpHeader,
	        "GET %s?type=folder HTTP/1.1\r\nDate: %s\r\nAccept: text/plain\r\nAccept-Encoding: identity\r\n\r\n",
	        a.remotePath, date);

	// send HTTP request to server
	if ( send(sock , httpHeader , strlen(httpHeader) , 0) < 0) {
		fprintf(stderr, "%s", "Unknown error.\n");
		exit(1);
	}

	// Receive a message with response from server
	char message[BUFF_SIZE];
	recv(sock , message , BUFF_SIZE , 0);

	// read code from HTTP response, if not OK - exit with certain error
	char code[12];
	strncpy(code, message, 12);
	code[12] = '\0';

	if (strcmp(code, "HTTP/1.1 404") == 0) {
		fprintf(stderr, "%s", "Directory not found.\n");
		exit(1);
	}
	if (strcmp(code, "HTTP/1.1 400") == 0) {
		fprintf(stderr, "%s", "Not a directory.\n");
		exit(1);
	}
	if (strcmp(code, "HTTP/1.1 401") == 0) {
		fprintf(stderr, "%s", "User Account Not Found.\n");
		exit(1);
	}

	// send "ready"
	memset(message, 0, strlen(message));
	sprintf(message, "ready");
	if ( send(sock , message , strlen(message) , 0) < 0) {
		fprintf(stderr, "%s", "Unknown error.\n");
		exit(1);
	}

	// Receive a message with list segments (ls) from server
	recv(sock , message , BUFF_SIZE , 0);

	// print ls
	printf("%s\n", message);
}

/**
 * @brief      Get the file from the server
 *
 * @param[in]  sock  The socket
 * @param[in]  a     arguments
 */
void get(int sock, arguments a) {

	// HTTP: Date
	char date[1000];
	time_t now = time(0);
	struct tm tm = *gmtime(&now);
	strftime(date, sizeof date, "%a, %d %b %Y %H:%M:%S %Z", &tm);

	// prepare HTTP request
	char httpHeader[BUFF_SIZE];
	sprintf(httpHeader,
	        "GET %s?type=file HTTP/1.1\r\nDate: %s\r\nAccept: text/plain\r\nAccept-Encoding: identity\r\n\r\n",
	        a.remotePath, date);

	// send HTTP request to server
	if ( send(sock , httpHeader , strlen(httpHeader) , 0) < 0) {
		fprintf(stderr, "%s", "Unknown error.\n");
		exit(1);
	}

	// Receive a message with response from server
	char message[BUFF_SIZE];
	recv(sock , message , BUFF_SIZE , 0);

	// read code from HTTP response, if not OK - exit with certain error
	char code[12];
	strncpy(code, message, 12);
	code[12] = '\0';

	if (strcmp(code, "HTTP/1.1 404") == 0) {
		fprintf(stderr, "%s", "File not found.\n");
		exit(1);
	}
	if (strcmp(code, "HTTP/1.1 400") == 0) {
		fprintf(stderr, "%s", "Not a file.\n");
		exit(1);
	}
	if (strcmp(code, "HTTP/1.1 401") == 0) {
		fprintf(stderr, "%s", "User Account Not Found.\n");
		exit(1);
	}
	// else OK:

	// get lenght from HTTP header
	char number[20];
	// beg index
	int beg;
	for (beg = strlen(message) - 1; beg > 0; beg--) {
		if (message[beg] == ':') {
			break;
		}
	}
	beg = beg + 2;
	// end index
	int end;
	for (end = strlen(message) - 1; end > 0; end--) {
		if (message[end] == '.') {
			break;
		}
	}
	end = end - 1;
	// strncpy(dest, src + beginIndex, endIndex - beginIndex);
	strncpy(number, message + beg, end - beg + 1);
	number[end - beg + 2] = '\0';

	int size = atoi(number);

	// create new file
	FILE *fw;
	fw = fopen(a.localPath, "w");
	// send "ready"
	memset(message, 0, strlen(message));
	sprintf(message, "ready");
	if ( send(sock , message , strlen(message) , 0) < 0) {
		fprintf(stderr, "%s", "Unknown error.\n");
		exit(1);
	}

	// clean buffer
	memset(message, 0, strlen(message));

	// receaved data - counter
	int recieved = 0;
	// write to file
	while ((recieved + BUFF_SIZE) < size ) {
		// receave
		recv(sock, message, BUFF_SIZE, 0);
		// write data (part of datas) to file
		fwrite(message, BUFF_SIZE, 1, fw);
		// clean buff
		memset(message, 0, strlen(message));
		// count all receaved data
		recieved += BUFF_SIZE;
	}
	// end part of data
	if (recieved < size) {
		// clean buff
		memset(message, 0, strlen(message));
		// receave last part of data
		recv(sock, message, (size - recieved), 0);
		// write last part of data
		fwrite(message, (size - recieved), 1, fw);  
	}

	// close the file
	fclose(fw);
}

/**
 * @brief      The function put uploads a file from the local path on the remote
 *             server path.
 *
 * @param[in]  sock  The socket
 * @param[in]  a     arguments
 */
void put(int sock, arguments a) {

	// Open file
	FILE *fr;
	fr = fopen(a.localPath, "r");
	// file does not exist
	if (fr == NULL) {
		fprintf(stderr, "%s", "Unknown error.\n");
		exit(1);
	}

	// HTTP: Content-Length
	struct stat st;
	stat(a.localPath, &st);
	int size = st.st_size;

	// HTTP: Date
	char date[1000];
	time_t now = time(0);
	struct tm tm = *gmtime(&now);
	strftime(date, sizeof date, "%a, %d %b %Y %H:%M:%S %Z", &tm);

	// prepare HTTP request
	char httpHeader[BUFF_SIZE];
	sprintf(httpHeader,
	        "PUT %s?type=file HTTP/1.1\r\nDate: %s\r\nAccept: text/plain\r\nAccept-Encoding: identity\r\nContent-Type: text/plain\r\nContent-Length: %d.\r\n\r\n",
	        a.remotePath, date, size);

	// send HTTP request to server
	if ( send(sock , httpHeader , strlen(httpHeader) , 0) < 0) {
		fprintf(stderr, "%s", "Unknown error.\n");
		exit(1);
	}

	// Receive a message with response from server
	char message[BUFF_SIZE];
	recv(sock , message , BUFF_SIZE , 0);

	// read code from HTTP response, if not OK - exit with certain error
	char code[12];
	strncpy(code, message, 12);
	code[12] = '\0';

	if (strcmp(code, "HTTP/1.1 400") == 0) {
		fprintf(stderr, "%s", "Already exists.\n");
		exit(1);
	}
	if (strcmp(code, "HTTP/1.1 409") == 0) {
		fprintf(stderr, "%s", "Unknown error.\n");
		exit(1);
	}
	if (strcmp(code, "HTTP/1.1 401") == 0) {
		fprintf(stderr, "%s", "User Account Not Found.\n");
		exit(1);
	}

	// clean buffer
	memset(message, 0, strlen(message));
	// sended data - counter
	int sended = 0;

	// Send file
	while ((sended + BUFF_SIZE) < size) {
		// read part of data
		fread(message, BUFF_SIZE, 1, fr);
		// send BUFF_SIZE bytes
		if ( send(sock, message, BUFF_SIZE, 0) < 0) {
			fprintf(stderr, "%s", "Unknown error.\n");
			exit(1);
		}
		// add sending data to the counter sended
		sended += BUFF_SIZE;
		// clean buff
		memset(message, 0, strlen(message));
	}
	// sending last part of data
	if (sended < size) {
		// clean buff
		memset(message, 0, strlen(message));
		// read last part of data
		fread(message, (size - sended), 1, fr);
		// send last part of data
		if ( send(sock, message, BUFF_SIZE, 0) < 0) {
			fprintf(stderr, "%s", "Unknown error.\n");
			exit(1);
		}
	}

	// Close file
	fclose(fr);
}

/**
 * @brief      Simply function to processing arguments
 *
 * @param[in]  argc  The argc
 * @param      argv  The argv
 *
 * @return     struct arguments with port and path
 */
arguments parameterProcessing(int argc , char *argv[]) {

	// struct arguments
	arguments a;

	if (argc == 3 || argc == 4) {

		// COMAND
		if (strcmp(argv[1], "del") == 0) {
			strcpy(a.command, argv[1]);
		}
		else if (strcmp(argv[1], "get") == 0) {
			strcpy(a.command, argv[1]);
		}
		else if (strcmp(argv[1], "put") == 0) {
			strcpy(a.command, argv[1]);
		}
		else if (strcmp(argv[1], "lst") == 0) {
			strcpy(a.command, argv[1]);
		}
		else if (strcmp(argv[1], "mkd") == 0) {
			strcpy(a.command, argv[1]);
		}
		else if (strcmp(argv[1], "rmd") == 0) {
			strcpy(a.command, argv[1]);
		}
		else {
			fprintf(stderr, "Wrong arguments!\n");
			exit(1);
		}

		// REMOTE-PATH
		char tmp[100];
		strcpy(tmp, argv[2]);
		int slashCount = 0;
		int j = 0;
		bool flag = false;
		char tmpBuff[50];

		for (int i = 0; tmp[i] != '\0'; i++) {

			if ((slashCount == 2) && (flag == false)) {
				if (tmp[i] == ':') {
					a.host[j] = '\0';
					flag = true;
					j = 0;
					continue;
				}
				else {
					a.host[j] = tmp[i];
					j++;
				}
			}
			// localhost transfare to 127.0.0.1
			if (strcmp(a.host, "localhost") == 0) {
				memset(a.host, 0, strlen(a.host));
				strcpy(a.host, "127.0.0.1");
			}

			if (flag) {

				if (tmp[i] == '/') {
					tmpBuff[j] = '\0';
					a.port = atoi(tmpBuff);
					flag = true;

					int x;
					for (x = 0; tmp[x + i] != '\0'; x++ ) {
						a.remotePath[x] = tmp[x + i];
					}

					a.remotePath[x + 1] = '\0';
					break;

				}
				else {
					tmpBuff[j] = tmp[i];
					j++;
				}

			}

			if (tmp[i] == '/') {
				slashCount++;
			}
		}

		// LOCAL-PATH
		if (argc == 4) {
			strcpy(a.localPath, argv[3]);

		}
		// deufald - local path
		else if (strcmp(argv[1], "get") == 0) {
			// get folder path from path
			int y;
			for (y = strlen(a.remotePath) - 1; y > 0; y--) {
				if (a.remotePath[y] == '/') {
					break;
				}
			}
			strcpy(a.localPath, "./");
			// strncpy(dest, src + beginIndex, endIndex - beginIndex);
			strncpy(a.localPath + 2, a.remotePath + y + 1, strlen(a.remotePath) - y);
			a.localPath[strlen(a.remotePath) - y + 2] = '\0';
		}
		else {
			strcpy(a.localPath, "");
		}
	}
	else {
		fprintf(stderr, "Wrong arguments!!\n");
		exit(1);
	}

	// return struct with port and path
	return a;
}