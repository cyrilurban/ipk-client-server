# Dokumentace k projektu 1 do přědmětu IPK

 | IPK   | Projekt 1    |
 | ------| ------------ |
 | jméno | Cyril Urban  |
 | login | xurban65     |
 | datum | 2017-03-14   |
 
### 1 Úvod
Klient/server aplikace pro přenos souborů, která komunikuje pomocí HTTP a používá jednoduché RESTful API. Aplikace je implementována v programovacím jazyce C. 
**Klient** se jmenuje ftrest a **server** má název ftrestd, ke komunikaci mezi klientem a serverem je využita funkce socket.

### 2 Klient
Klient zkontroluje správnost uživatelských argumentů a zpracuje je - naplní strukturu arguments, kterou vrátí jako výsledek do main funkce. Na základě techto argumentů je nastaven port a název serveru, na který bude záslán požadavek. Dále je podle příkazu volána jedna z funkcí, která tento příkaz reprezentuje:

  - **del:** smaže soubor na serveru
  - **get:** zkopíruje soubor ze serveru do aktuálního lokálního adresáře
  - **put:** zkopíruje soubor na server
  - **lst:** vypíše obsah vzdáleného adresáře na standardní výstup
  - **mkd:** vytvoří adresář na serveru
  - **rmd:** odstraní adresář ze serveru

Každá z těchto funkcí vytvoří specifickou HTTP hlavičku podle jejich požadavků a pošle ji jako požadavek serveru. Dále čeká na odpověď serveru. Bezprostředně po přijetí odpovědi, program dešifruje přijatou HTTP hlavičku odpovědi. Podle dodaných indícií dále pokračuje. Buď dokonči svůj úkol, například vypíše seznam souborů na standardní výstup a úspěšně skončí nebo je na chybový výstup vypsána určitá chybová hláška.

### 3 Server
Při spuštění serveru jsou stejným stylem jako v klientovi zkontrolovány a zpracovány argumenty, které dále specifikují na jakém portu bude server naslouchat a jaký bude jeho kořenový adresář. Server dále čeká na připojení klienta. Klient mu po připojení pošle požadavek s HTTP hlavičkou, tato zpráva je zpracována v oddělené funkci, která následně vrací naplněnou strukturu s výsledky požadavku. Dále je volána jedna z funkcí podobně jako v klientovi (každá funkce reprezentuje jeden příkaz pro odpoveď). V jednotlivých funkcích je otestováno, zda může být příkaz podle daných parametrů proveden. V případě úspěchu se vykoná příslušná operace a je posláná odpověď s HTTP hlavičkou představující úspěch (200 OK). V  případě neúspěchů je poslána HTTP hlavička, která nese mimo jiné jeden z chybových kódú, na základě kterých oznámí klientovi důvod neúspěchu. Tato chyba je vypsána taktéž na standardní chybový výstup serveru.