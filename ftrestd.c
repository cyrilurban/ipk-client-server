/**
 * @file	ftrestd.c
 * @author	CYRIL URBAN
 * @date:	2017-03-14
 * @brief	The client/server application
 */

#include "ftrestd.h"

/**
 * @brief      The main function of server
 *
 * @param[in]  argc  The argc
 * @param      argv  The argv
 *
 * @return     0 or 1
 */
int main(int argc , char *argv[])
{
	// set port and root folder from arguments, call function parameterProcessing
	arguments a;
	a = parameterProcessing(argc , argv);

	int socket_desc , client_sock , c , read_size;
	struct sockaddr_in server , client;
	char client_message[BUFF_SIZE];

	// Create socket
	socket_desc = socket(AF_INET , SOCK_STREAM , 0);
	if (socket_desc == -1) {
		fprintf(stderr, "%s", "Unknown error.\n");
		exit(1);
	}

	// Prepare the sockaddr_in structure
	server.sin_family = AF_INET;
	server.sin_addr.s_addr = INADDR_ANY;
	server.sin_port = htons(a.port);

	// Bind
	if (bind(socket_desc, (struct sockaddr *)&server , sizeof(server)) < 0) {
		fprintf(stderr, "%s", "Unknown error.\n");
		exit(1);
	}

	// Listen
	listen(socket_desc , 3);

	// Accept and incoming connection
	c = sizeof(struct sockaddr_in);

	// accept connection from an incoming client
	client_sock = accept(socket_desc, (struct sockaddr *)&client, (socklen_t*)&c);
	if (client_sock < 0) {
		fprintf(stderr, "%s", "Unknown error.\n");
		exit(1);
	}

	// Receive a COMMAND message from client
	read_size = recv(client_sock , client_message , BUFF_SIZE , 0);
	if (read_size < 1) {
		fprintf(stderr, "%s", "Unknown error.\n");
		exit(1);
	}

	clientArg clientArg;
	// encode HTTP message from client
	clientArg = encodeHTTP(client_message, a, client_sock);

	// COMAND - call certain function
	// put
	if (strcmp(clientArg.command, "put") == 0) {
		put(client_sock, a, clientArg);
	}
	else if (strcmp(clientArg.command, "lst") == 0) {
		lst(client_sock, a, clientArg);
	}
	// mkd
	else if (strcmp(clientArg.command, "mkd") == 0) {
		mkd(client_sock, a, clientArg);
	}
	// rmd
	else if (strcmp(clientArg.command, "rmd") == 0) {
		rmd(client_sock, a, clientArg);
	}
	// del
	else if (strcmp(clientArg.command, "del") == 0) {
		del(client_sock, a, clientArg);
	}
	// get
	else if (strcmp(clientArg.command, "get") == 0) {
		get(client_sock, a, clientArg);
	}

	// Close sock and successfully exit
	close(client_sock);
	return 0;
}


/**
 * @brief      encoding HTTP header
 *
 * @param      httpHeader   The http header
 * @param[in]  a            The arguments
 * @param[in]  client_sock  The client sock
 *
 * @return     c			The result struct with command (function) and path
 */
clientArg encodeHTTP(char httpHeader[BUFF_SIZE], arguments a, int client_sock) {

	// struct with arguments
	clientArg c;

	char type[4];

	// get rest command from http header (first 3 chars)
	// GET || PUT || DELETE
	char rest[3];
	strncpy(rest, httpHeader, 3);
	rest[3] = '\0';

	// DELETE
	if (strcmp(rest, "DEL") == 0) {
		// position of '?'
		int i;
		for (i = 0; i < strlen(httpHeader); i++) {
			if (httpHeader[i] == '?') {
				break;
			}
		}
		// get path from URI
		// strncpy(dest, src + beginIndex, endIndex - beginIndex);
		strncpy(c.path, httpHeader + 7, i - 7);
		c.path[i - 7] = '\0';

		// get type from URI
		// folder || file
		strncpy(type, httpHeader + i + 6, 4);
		type[4] = '\0';
	}
	// GET || PUT
	else {
		// position of '?'
		int i;
		for (i = 0; i < strlen(httpHeader); i++) {
			if (httpHeader[i] == '?') {
				break;
			}
		}
		// get path from URI
		// strncpy(dest, src + beginIndex, endIndex - beginIndex);
		strncpy(c.path, httpHeader + 4, i - 4);
		c.path[i - 4] = '\0';

		// get type from URI
		// folder || file
		strncpy(type, httpHeader + i + 6, 4);
		type[4] = '\0';
	}


	// Exists user - "User Account Not Found"?
	char name[100];
	// position of '/'
	int y;
	for (y = 1; y < strlen(c.path); y++) {
		if (c.path[y] == '/') {
			break;
		}
	}
	// get name from path
	// strncpy(dest, src + beginIndex, endIndex - beginIndex);
	strncpy(name, c.path + 1, y - 1);
	name[y] = '\0';

	bool findUser = false;
	DIR *dir;
	struct dirent *ent;
	// directory exists
	if ((dir = opendir(a.rootFolder)) != NULL) {
		// print all the files and directories within directory
		while ((ent = readdir (dir)) != NULL) {
			// ignor files "." and ".."
			if ((strcmp(ent->d_name, ".") != 0)  && (strcmp(ent->d_name, "..") != 0)) {
				// compare names from root with user name
				if (strcmp(ent->d_name, name) == 0) {
					findUser = true;
					break;
				}
			}
		}
		closedir(dir);
	}

	// "User Account Not Found"
	if (findUser == false) {

		// it will be string with HTTP response
		char httpMessage[BUFF_SIZE];

		// HTTP: Date
		char date[1000];
		time_t now = time(0);
		struct tm tm = *gmtime(&now);
		strftime(date, sizeof date, "%a, %d %b %Y %H:%M:%S %Z", &tm);

		// prepare HTTP response
		sprintf(httpMessage,
		        "HTTP/1.1 401 Unauthorized\r\nDate: %s\r\nContent-Type: text/plain\r\nContent-Length: 0\r\nContent-Encoding: identity\r\n\r\n",
		        date);
		// send message with HTTP response
		if ( send(client_sock , httpMessage , strlen(httpMessage) , 0) < 0) {
			fprintf(stderr, "%s", "Unknown error.\n");
			exit(1);
		}
		// stderr
		fprintf(stderr, "%s", "User Account Not Found.\n");
		exit(1);
	}

	// else user was founded - continue
	if ((strcmp(rest, "PUT") == 0) && (strcmp(type, "file") == 0)) {

		// get lenght
		char number[20];

		// beg index
		int beg;
		for (beg = strlen(httpHeader) - 1; beg > 0; beg--) {
			if (httpHeader[beg] == ':') {
				break;
			}
		}
		beg = beg + 2;

		// end index
		int end;
		for (end = strlen(httpHeader) - 1; end > 0; end--) {
			if (httpHeader[end] == '.') {
				break;
			}
		}
		end = end - 1;

		// strncpy(dest, src + beginIndex, endIndex - beginIndex);
		strncpy(number, httpHeader + beg, end - beg + 1);
		number[end - beg + 2] = '\0';

		c.size = atoi(number);
		strcpy(c.command, "put");
	}
	else if ((strcmp(rest, "PUT") == 0) && (strcmp(type, "fold") == 0)) {
		strcpy(c.command, "mkd");
	}
	else if ((strcmp(rest, "GET") == 0) && (strcmp(type, "file") == 0)) {
		strcpy(c.command, "get");
	}
	else if ((strcmp(rest, "GET") == 0) && (strcmp(type, "fold") == 0)) {
		strcpy(c.command, "lst");
	}
	else if ((strcmp(rest, "DEL") == 0) && (strcmp(type, "file") == 0)) {
		strcpy(c.command, "del");
	}
	else if ((strcmp(rest, "DEL") == 0) && (strcmp(type, "fold") == 0)) {

		// delte user?

		char slashName[100];
		// '/' + name
		strcpy(slashName, "/");
		strcat(slashName, name);

		// "The user cannot be deleted."
		if ((strcmp(slashName, c.path) == 0)) {

			// it will be string with HTTP response
			char httpMessage[BUFF_SIZE];

			// HTTP: Date
			char date[1000];
			time_t now = time(0);
			struct tm tm = *gmtime(&now);
			strftime(date, sizeof date, "%a, %d %b %Y %H:%M:%S %Z", &tm);

			// prepare HTTP response
			sprintf(httpMessage,
			        "HTTP/1.1 422 Unprocessable Entity\r\nDate: %s\r\nContent-Type: text/plain\r\nContent-Length: 0\r\nContent-Encoding: identity\r\n\r\n",
			        date);
			// send message with HTTP response
			if ( send(client_sock , httpMessage , strlen(httpMessage) , 0) < 0) {
				fprintf(stderr, "%s", "Unknown error.\n");
				exit(1);
			}
			// stderr
			fprintf(stderr, "%s", "The user cannot be deleted.\n");
			exit(1);
		}

		// else
		strcpy(c.command, "rmd");
	}

	// return result struct with command (function) and path
	return c;
}

/**
 * @brief      Send the file to client (from server)
 *
 * @param[in]  client_sock  The client socket
 * @param[in]  a            The arguments
 * @param[in]  clientArg    The client argument
 */
void get(int client_sock, arguments a, clientArg clientArg) {

	// it will be string with HTTP response
	char httpMessage[BUFF_SIZE];

	// HTTP: Date
	char date[1000];
	time_t now = time(0);
	struct tm tm = *gmtime(&now);
	strftime(date, sizeof date, "%a, %d %b %Y %H:%M:%S %Z", &tm);

	// allocate: root folder + path  + 1 for the zero-terminator
	char *path = malloc(strlen(a.rootFolder) + strlen(clientArg.path) + 1);
	if (path == NULL) {
		fprintf(stderr, "%s", "Unknown error.\n");
		exit(1);
	}

	// root folder + path
	strcpy(path, a.rootFolder);
	strcat(path, clientArg.path);

	// HTTP: Content-Length
	struct stat st;
	stat(path, &st);
	int size = st.st_size;

	// try if path is directory
	DIR *dir;
	struct dirent *ent;
	// if directory exists. Send error 400
	if ((dir = opendir(path)) != NULL) {
		// prepare HTTP response
		sprintf(httpMessage,
		        "HTTP/1.1 400 Bad Request\r\nDate: %s\r\nContent-Type: text/plain\r\nContent-Length: 0\r\nContent-Encoding: identity\r\n\r\n",
		        date);
		// send message with HTTP response
		if ( send(client_sock , httpMessage , strlen(httpMessage) , 0) < 0) {
			closedir(dir);
			fprintf(stderr, "%s", "Unknown error.\n");
			exit(1);
		}
		// stderr
		fprintf(stderr, "%s", "Not a file.\n");
		closedir(dir);
		exit(1);
	}
	// OK || "File not found."
	else {
		// try open the file
		FILE *fr;
		fr = fopen(path, "r");
		// if file exists, it will be send. First send: OK 200
		if (fr != NULL) {
			// close the file
			fclose(fr);

			// prepare HTTP (OK) response
			sprintf(httpMessage,
			        "HTTP/1.1 200 OK\r\nDate: %s\r\nContent-Type: text/plain\r\nContent-Encoding: identity\r\nContent-Length: %d.\r\n\r\n",
			        date, size);
			// send message with HTTP response
			if ( send(client_sock , httpMessage , strlen(httpMessage) , 0) < 0) {
				fprintf(stderr, "%s", "Unknown error.\n");
				exit(1);
			}
		}
		// "Directory not found."
		else {
			// prepare HTTP response
			sprintf(httpMessage,
			        "HTTP/1.1 404 Not Found\r\nDate: %s\r\nContent-Type: text/plain\r\nContent-Length: 0\r\nContent-Encoding: identity\r\n\r\n",
			        date);
			// send message with HTTP response
			if ( send(client_sock , httpMessage , strlen(httpMessage) , 0) < 0) {
				fprintf(stderr, "%s", "Unknown error.\n");
				exit(1);
			}
			// stderr
			fprintf(stderr, "%s", "File not found.\n");
			exit(1);
		}
	}

	// receave confirmation message (ready)
	char confirmation[BUFF_SIZE];
	recv(client_sock, confirmation, BUFF_SIZE, 0);
	if (strcmp(confirmation, "ready") != 0) {
		exit(1);
	}

	// open the file
	FILE *fr;
	fr = fopen(path, "r");

	// sended data - counter
	int sended = 0;

	// Send file
	char message[BUFF_SIZE];
	while ((sended + BUFF_SIZE) < size) {
		// read part of data
		fread(message, BUFF_SIZE, 1, fr);
		// send BUFF_SIZE bytes
		if ( send(client_sock, message, BUFF_SIZE, 0) < 0) {
			fprintf(stderr, "%s", "Unknown error.\n");
			exit(1);
		}
		// add sending data to the counter sended
		sended += BUFF_SIZE;
		// clean buff
		memset(message, 0, strlen(message));
	}
	// sending last part of data
	if (sended < size) {
		// clean buff
		memset(message, 0, strlen(message));
		// read last part of data
		fread(message, (size - sended), 1, fr);
		// send last part of data
		if ( send(client_sock, message, BUFF_SIZE, 0) < 0) {
			fprintf(stderr, "%s", "Unknown error.\n");
			exit(1);
		}
	}

	// close the file
	fclose(fr);
}

/**
 * @brief      The mkd function makes directory
 *
 * @param[in]  client_sock  The client socket
 * @param[in]  a            The arguments
 * @param[in]  clientArg    The client argument
 */
void mkd(int client_sock, arguments a, clientArg clientArg) {

	// it will be string with HTTP response
	char httpMessage[BUFF_SIZE];

	// HTTP: Date
	char date[1000];
	time_t now = time(0);
	struct tm tm = *gmtime(&now);
	strftime(date, sizeof date, "%a, %d %b %Y %H:%M:%S %Z", &tm);

	// allocate: root folder + path  + 1 for the zero-terminator
	char *path = malloc(strlen(a.rootFolder) + strlen(clientArg.path) + 1);
	if (path == NULL) {
		fprintf(stderr, "%s", "Unknown error.\n");
		exit(1);
	}

	// root folder + path
	strcpy(path, a.rootFolder);
	strcat(path, clientArg.path);

	// try open the file or folder
	FILE *fr;
	fr = fopen(path, "r");
	// 400 Bad Request - "Already exists."
	if (fr != NULL) {
		// prepare HTTP response
		sprintf(httpMessage,
		        "HTTP/1.1 400 Bad Request\r\nDate: %s\r\nContent-Type: text/plain\r\nContent-Length: 0\r\nContent-Encoding: identity\r\n\r\n",
		        date);
		// send message with HTTP response
		if ( send(client_sock , httpMessage , strlen(httpMessage) , 0) < 0) {
			fclose(fr);
			fprintf(stderr, "%s", "Unknown error.\n");
			exit(1);
		}
		// stderr
		fprintf(stderr, "%s", "Already exists.\n");
		fclose(fr);
		exit(1);
	}
	// OK, execute mkd
	else {
		// make a directory
		mkdir(path, 0777);
		// prepare HTTP (OK) response
		sprintf(httpMessage,
		        "HTTP/1.1 200 OK\r\nDate: %s\r\nContent-Type: text/plain\r\nContent-Length: 0\r\nContent-Encoding: identity\r\n\r\n",
		        date);
		// send message with HTTP response
		if ( send(client_sock , httpMessage , strlen(httpMessage) , 0) < 0) {
			fprintf(stderr, "%s", "Unknown error.\n");
			exit(1);
		}
	}
}

/**
 * @brief      The rmd function deletes a directory
 *
 * @param[in]  client_sock  The client socket
 * @param[in]  a            The arguments
 * @param[in]  clientArg    The client argument
 */
void rmd(int client_sock, arguments a, clientArg clientArg) {

	// it will be string with HTTP response
	char httpMessage[BUFF_SIZE];

	// HTTP: Date
	char date[1000];
	time_t now = time(0);
	struct tm tm = *gmtime(&now);
	strftime(date, sizeof date, "%a, %d %b %Y %H:%M:%S %Z", &tm);

	// allocate: root folder + path  + 1 for the zero-terminator
	char *path = malloc(strlen(a.rootFolder) + strlen(clientArg.path) + 1);
	if (path == NULL) {
		fprintf(stderr, "%s", "Unknown error.\n");
		exit(1);
	}

	// root folder + path
	strcpy(path, a.rootFolder);
	strcat(path, clientArg.path);

	// try if path is a directory
	DIR *dir;
	struct dirent *ent;
	// if directory exists, it will be deleted and send OK (if not empty)
	if ((dir = opendir(path)) != NULL) {
		// check if dir is empty
		while ((ent = readdir (dir)) != NULL) {
			// ignor files "." and ".."
			if ((strcmp(ent->d_name, ".") != 0)  && (strcmp(ent->d_name, "..") != 0)) {
				// dir is not empty - error 409 Conflict
				// prepare HTTP response
				sprintf(httpMessage,
				        "HTTP/1.1 409 Conflict\r\nDate: %s\r\nContent-Type: text/plain\r\nContent-Length: 0\r\nContent-Encoding: identity\r\n\r\n",
				        date);
				// send message with HTTP response
				if ( send(client_sock , httpMessage , strlen(httpMessage) , 0) < 0) {
					closedir(dir);
					fprintf(stderr, "%s", "Unknown error.\n");
					exit(1);
				}
				// stderr
				fprintf(stderr, "%s", "Directory not empty.\n");
				closedir(dir);
				exit(1);
			}
		}

		// close and delete folder
		closedir(dir);
		rmdir(path);

		// prepare HTTP (OK) response
		sprintf(httpMessage,
		        "HTTP/1.1 200 OK\r\nDate: %s\r\nContent-Type: text/plain\r\nContent-Length: 0\r\nContent-Encoding: identity\r\n\r\n",
		        date);

		// send message with HTTP response
		if ( send(client_sock , httpMessage , strlen(httpMessage) , 0) < 0) {
			fprintf(stderr, "%s", "Unknown error.\n");
			exit(1);
		}
	}
	// "Not a directory." || "Directory not found."
	else {
		// try open the file
		FILE *fr;
		fr = fopen(path, "r");
		// "Not a directory."
		if (fr != NULL) {
			// prepare HTTP response
			sprintf(httpMessage,
			        "HTTP/1.1 400 Bad Request\r\nDate: %s\r\nContent-Type: text/plain\r\nContent-Length: 0\r\nContent-Encoding: identity\r\n\r\n",
			        date);
			// send message with HTTP response
			if ( send(client_sock , httpMessage , strlen(httpMessage) , 0) < 0) {
				fclose(fr);
				fprintf(stderr, "%s", "Unknown error.\n");
				exit(1);
			}
			// stderr
			fprintf(stderr, "%s", "Not a directory.\n");
			fclose(fr);
			exit(1);
		}
		// "Directory not found."
		else {
			// prepare HTTP response
			sprintf(httpMessage,
			        "HTTP/1.1 404 Not Found\r\nDate: %s\r\nContent-Type: text/plain\r\nContent-Length: 0\r\nContent-Encoding: identity\r\n\r\n",
			        date);
			// send message with HTTP response
			if ( send(client_sock , httpMessage , strlen(httpMessage) , 0) < 0) {
				fprintf(stderr, "%s", "Unknown error.\n");
				exit(1);
			}
			// stderr
			fprintf(stderr, "%s", "Directory not found.\n");
			exit(1);
		}
	}
}

/**
 * @brief      The del function deletes file
 *
 * @param[in]  client_sock  The client socket
 * @param[in]  a            The arguments
 * @param[in]  clientArg    The client argument
 */
void del(int client_sock, arguments a, clientArg clientArg) {

	// it will be string with HTTP response
	char httpMessage[BUFF_SIZE];

	// HTTP: Date
	char date[1000];
	time_t now = time(0);
	struct tm tm = *gmtime(&now);
	strftime(date, sizeof date, "%a, %d %b %Y %H:%M:%S %Z", &tm);

	// allocate: root folder + path  + 1 for the zero-terminator
	char *path = malloc(strlen(a.rootFolder) + strlen(clientArg.path) + 1);
	if (path == NULL) {
		fprintf(stderr, "%s", "Unknown error.\n");
		exit(1);
	}

	// root folder + path
	strcpy(path, a.rootFolder);
	strcat(path, clientArg.path);

	// try if path is directory
	DIR *dir;
	struct dirent *ent;
	// if directory exists. Send error 400
	if ((dir = opendir(path)) != NULL) {
		// prepare HTTP response
		sprintf(httpMessage,
		        "HTTP/1.1 400 Bad Request\r\nDate: %s\r\nContent-Type: text/plain\r\nContent-Length: 0\r\nContent-Encoding: identity\r\n\r\n",
		        date);
		// send message with HTTP response
		if ( send(client_sock , httpMessage , strlen(httpMessage) , 0) < 0) {
			closedir(dir);
			fprintf(stderr, "%s", "Unknown error.\n");
			exit(1);
		}
		// stderr
		fprintf(stderr, "%s", "Not a file.\n");
		closedir(dir);
		exit(1);
	}
	// OK || "File not found."
	else {
		// try open the file
		FILE *fr;
		fr = fopen(path, "r");
		// if file exists, it will be deleted. Send: OK 200
		if (fr != NULL) {
			// close and delete
			fclose(fr);
			unlink(path);

			// prepare HTTP (OK) response
			sprintf(httpMessage,
			        "HTTP/1.1 200 OK\r\nDate: %s\r\nContent-Type: text/plain\r\nContent-Length: 0\r\nContent-Encoding: identity\r\n\r\n",
			        date);

			// send message with HTTP response
			if ( send(client_sock , httpMessage , strlen(httpMessage) , 0) < 0) {
				fprintf(stderr, "%s", "Unknown error.\n");
				exit(1);
			}
		}
		// "Directory not found."
		else {
			// prepare HTTP response
			sprintf(httpMessage,
			        "HTTP/1.1 404 Not Found\r\nDate: %s\r\nContent-Type: text/plain\r\nContent-Length: 0\r\nContent-Encoding: identity\r\n\r\n",
			        date);
			// send message with HTTP response
			if ( send(client_sock , httpMessage , strlen(httpMessage) , 0) < 0) {
				fprintf(stderr, "%s", "Unknown error.\n");
				exit(1);
			}
			// stderr
			fprintf(stderr, "%s", "File not found.\n");
			exit(1);
		}
	}
}

/**
 * @brief      The function finds out list segments (ls) and sends it back to
 *             the client.
 *
 * @param[in]  client_sock  The client sock
 * @param[in]  a            The arguments
 * @param[in]  clientArg    The client argument
 */
void lst(int client_sock, arguments a, clientArg clientArg) {

	// it will be string with result - ls
	char client_message[BUFF_SIZE];
	// it will be string with HTTP response
	char httpMessage[BUFF_SIZE];

	// HTTP: Date
	char date[1000];
	time_t now = time(0);
	struct tm tm = *gmtime(&now);
	strftime(date, sizeof date, "%a, %d %b %Y %H:%M:%S %Z", &tm);

	// allocate: root folder + path  + 1 for the zero-terminator
	char *path = malloc(strlen(a.rootFolder) + strlen(clientArg.path) + 1);
	if (path == NULL) {
		fprintf(stderr, "%s", "Unknown error.\n");
		exit(1);
	}

	// root folder + path
	strcpy(path, a.rootFolder);
	strcat(path, clientArg.path);

	// execute ls
	DIR *dir;
	struct dirent *ent;
	// directory exists
	if ((dir = opendir(path)) != NULL) {
		// print all the files and directories within directory
		while ((ent = readdir (dir)) != NULL) {
			// ignor files "." and ".."
			if ((strcmp(ent->d_name, ".") != 0)  && (strcmp(ent->d_name, "..") != 0)) {
				strcat(client_message, ent->d_name);
				strcat(client_message, " ");
			}
		}
		closedir(dir);
		client_message[strlen(client_message)-1] = '\0';
	}
	// "Not a directory." || "Directory not found."
	else {
		// try open the file
		FILE *fr;
		fr = fopen(path, "r");
		// "Not a directory."
		if (fr != NULL) {
			// prepare HTTP response
			sprintf(httpMessage,
			        "HTTP/1.1 400 Bad Request\r\nDate: %s\r\nContent-Type: text/plain\r\nContent-Length: 0\r\nContent-Encoding: identity\r\n\r\n",
			        date);
			// send message with HTTP response
			if ( send(client_sock , httpMessage , strlen(httpMessage) , 0) < 0) {
				fclose(fr);
				fprintf(stderr, "%s", "Unknown error.\n");
				exit(1);
			}
			// stderr
			fprintf(stderr, "%s", "Not a directory.\n");
			fclose(fr);
			exit(1);
		}
		// "Directory not found."
		else {
			// prepare HTTP response
			sprintf(httpMessage,
			        "HTTP/1.1 404 Not Found\r\nDate: %s\r\nContent-Type: text/plain\r\nContent-Length: 0\r\nContent-Encoding: identity\r\n\r\n",
			        date);
			// send message with HTTP response
			if ( send(client_sock , httpMessage , strlen(httpMessage) , 0) < 0) {
				fprintf(stderr, "%s", "Unknown error.\n");
				exit(1);
			}
			// stderr
			fprintf(stderr, "%s", "Directory not found.\n");
			exit(1);
		}
	}

	// prepare HTTP (OK) response
	sprintf(httpMessage,
	        "HTTP/1.1 200 OK\r\nDate: %s\r\nContent-Type: text/plain\r\nContent-Length: %lu\r\nContent-Encoding: identity\r\n\r\n",
	        date, strlen(client_message));

	// send message with HTTP response
	if ( send(client_sock , httpMessage , strlen(httpMessage) , 0) < 0) {
		fprintf(stderr, "%s", "Unknown error.\n");
		exit(1);
	}

	// receave confirmation message (ready)
	char confirmation[BUFF_SIZE];
	recv(client_sock, confirmation, BUFF_SIZE, 0);
	if (strcmp(confirmation, "ready") != 0) {
		exit(1);
	}

	// send message with list segments (ls) to client
	if ( send(client_sock , client_message , BUFF_SIZE , 0) < 0) {
		fprintf(stderr, "%s", "Unknown error.\n");
		exit(1);
	}
}


/**
 * @brief      The function saves the client's file.
 *
 * @param[in]  client_sock  The client sock
 * @param[in]  a            The arguments
 * @param[in]  clientArg    The client argument
 */
void put(int client_sock, arguments a, clientArg clientArg) {

	// it will be string with HTTP response
	char httpMessage[BUFF_SIZE];

	// HTTP: Date
	char date[1000];
	time_t now = time(0);
	struct tm tm = *gmtime(&now);
	strftime(date, sizeof date, "%a, %d %b %Y %H:%M:%S %Z", &tm);

	// get path for new put file
	// allocate: root folder + path  + 1 for the zero-terminator
	char *path = malloc(strlen(a.rootFolder) + strlen(clientArg.path) + 1);
	if (path == NULL) {
		fprintf(stderr, "%s", "Unknown error.\n");
		exit(1);
	}
	// root folder + path
	strcpy(path, a.rootFolder);
	strcat(path, clientArg.path);

	// get folder path from path
	int i;
	for (i = strlen(path) - 1; i > 0; i--) {
		if (path[i] == '/') {
			break;
		}
	}
	// allocate: root folder + path  + 1 for the zero-terminator
	char *folderPath = malloc(i);
	if (folderPath == NULL) {
		fprintf(stderr, "%s", "Unknown error.\n");
		exit(1);
	}
	// strncpy(dest, src + beginIndex, endIndex - beginIndex);
	strncpy(folderPath, path, i);
	folderPath[i] = '\0';

	// try if folder path exists
	DIR *dir;
	struct dirent *ent;
	// if directory exists. Send error 409
	if ((dir = opendir(folderPath)) == NULL) {
		// dir does not exist - error 409 Conflict
		// prepare HTTP response
		sprintf(httpMessage,
		        "HTTP/1.1 409 Conflict\r\nDate: %s\r\nContent-Type: text/plain\r\nContent-Length: 0\r\nContent-Encoding: identity\r\n\r\n",
		        date);
		// send message with HTTP response
		if ( send(client_sock , httpMessage , strlen(httpMessage) , 0) < 0) {
			closedir(dir);
			fprintf(stderr, "%s", "Unknown error.\n");
			exit(1);
		}
		// stderr - Unknown error (Dir does not exist.)
		fprintf(stderr, "%s", "Unknown error.\n");
		closedir(dir);
		exit(1);
	}

	// try open the file or folder
	FILE *fr;
	fr = fopen(path, "r");
	// 400 Bad Request - "Already exists."
	if (fr != NULL) {
		// prepare HTTP response
		sprintf(httpMessage,
		        "HTTP/1.1 400 Bad Request\r\nDate: %s\r\nContent-Type: text/plain\r\nContent-Length: 0\r\nContent-Encoding: identity\r\n\r\n",
		        date);
		// send message with HTTP response
		if ( send(client_sock , httpMessage , strlen(httpMessage) , 0) < 0) {
			fclose(fr);
			fprintf(stderr, "%s", "Unknown error.\n");
			exit(1);
		}
		// stderr
		fprintf(stderr, "%s", "Already exists.\n");
		fclose(fr);
		exit(1);
	}
	// OK
	else {
		// prepare HTTP (OK) response
		sprintf(httpMessage,
		        "HTTP/1.1 200 OK\r\nDate: %s\r\nContent-Type: text/plain\r\nContent-Length: 0\r\nContent-Encoding: identity\r\n\r\n",
		        date);

		// send message with HTTP response
		if ( send(client_sock , httpMessage , strlen(httpMessage) , 0) < 0) {
			fprintf(stderr, "%s", "Unknown error.\n");
			exit(1);
		}
	}

	// create new file
	FILE *fw;
	fw = fopen(path, "w");

	char client_message[BUFF_SIZE];

	// receaved data - counter
	int recieved = 0;
	// write to file
	while ((recieved + BUFF_SIZE) < clientArg.size ) {
		// receave
		recv(client_sock, client_message, BUFF_SIZE, 0);
		// write data (part of datas) to file
		fwrite(client_message, BUFF_SIZE, 1, fw);
		// clean buff
		memset(client_message, 0, strlen(client_message));
		// count all receaved data
		recieved += BUFF_SIZE;
	}
	// end part of data
	if (recieved < clientArg.size) {
		// clean buff
		memset(client_message, 0, strlen(client_message));
		// receave last part of data
		recv(client_sock, client_message, (clientArg.size - recieved), 0);
		// write last part of data
		fwrite(client_message, (clientArg.size - recieved), 1, fw);
	}

	// close the file
	fclose(fw);
}

/**
 * @brief      Patameter processing - function to processing arguments
 *
 * @param[in]  argc  The argc
 * @param      argv  The argv
 *
 * @return     struct arguments with port and path
 */
arguments parameterProcessing(int argc , char *argv[]) {

	// struct arguments
	arguments a;

	// deufalt port and path
	a.port = 6677;
	strcpy(a.rootFolder, ".");

	if (argc > 1) {
		// -r
		if (strcmp(argv[1], "-r") == 0)  {
			// wrong arguments
			if (argc < 3) {
				fprintf(stderr, "Wrong arguments!\n");
				exit(1);
			}
			// ROOT-FOLDER
			strcpy(a.rootFolder, argv[2]);

			if (argc > 3) {
				// -p
				if (strcmp(argv[3], "-p") == 0)  {
					// wrong arguments
					if (argc != 5) {
						fprintf(stderr, "Wrong arguments!\n");
						exit(1);
					}
					// PORT
					a.port = atoi(argv[4]);
				}
			}
		}
		// -p
		else if (strcmp(argv[1], "-p") == 0)  {
			// wrong arguments
			if (argc != 3) {
				fprintf(stderr, "Wrong arguments!\n");
				exit(1);
			}
			// PORT
			a.port = atoi(argv[2]);

		}
		// wrong arguments
		else {
			fprintf(stderr, "Wrong arguments!\n");
			exit(1);
		}
	}

	// return struct with port and path
	return a;
}