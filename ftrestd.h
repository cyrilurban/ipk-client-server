/**
 * @file	ftrestd.h
 * @author	CYRIL URBAN
 * @date:	2017-03-14
 * @brief	The client/server application
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <ctype.h>
#include <sys/stat.h>
#include <dirent.h>
#include <time.h>
#include <sys/stat.h>
#include <stdbool.h>

// size of message buffer
#define BUFF_SIZE 2000

// struct with argumetns
typedef struct {
	char rootFolder[100];
	int port;
} arguments;

// struct with client argumetns - from HTTP header
typedef struct {
	char path[100];
	char command[3];
	int size;
} clientArg;

arguments parameterProcessing(int argc , char *argv[]);
clientArg encodeHTTP(char httpHeader[BUFF_SIZE], arguments a, int client_sock);
void put(int client_sock, arguments a, clientArg clientArg);
void lst(int client_sock, arguments a, clientArg clientArg);
void mkd(int client_sock, arguments a, clientArg clientArg);
void rmd(int client_sock, arguments a, clientArg clientArg);
void del(int client_sock, arguments a, clientArg clientArg);
void get(int client_sock, arguments a, clientArg clientArg);
